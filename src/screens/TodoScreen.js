import React, {useContext, useState} from "react";
import {View, StyleSheet, Dimensions} from "react-native";
import {FontAwesome, AntDesign} from '@expo/vector-icons';

import {THEME} from "../theme";
import {AppCard} from "../components/ui/AppCard";
import {EditModal} from "../components/EditModal";
import {AppTextBold} from "../components/ui/AppTextBold";
import {AppButton} from "../components/ui/AppButton";
import {TodoContext} from "../context/todo/todoContext";
import {ScreenContext} from "../context/screen/screenContext";

export const TodoScreen = () => {
    const {todos, updateTodo, removeTodo} = useContext(TodoContext);
    const {todoId, changeScreen} = useContext(ScreenContext);
    const [modal, setModal] = useState(false);
    const todo = todos.find(t => t.id === todoId);
    const saveHandler = async title => {
        await updateTodo(todo.id, title); //async await for closing Modal only after updeting finishing on server
        setModal(false);
    };

    return (
        <View>
            <EditModal value={todo.title}
                       visible={modal}
                       onCancel={() => setModal(false)}
                       onSave={saveHandler}/>
            <AppCard style={styles.card}>
                <AppTextBold style={styles.title}>{todo.title}</AppTextBold>
                <AppButton onPress={() => setModal(true)}>
                    <FontAwesome name='edit' size={20} color='#fff'/>
                </AppButton>
            </AppCard>
            <View style={styles.buttons}>
                <View style={styles.button}>
                    <AppButton color={THEME.GRAY_COLOR} onPress={() => changeScreen(null)}>
                        {/*<AppButton color={THEME.GRAY_COLOR} onPress={goBack}>*/}
                        <AntDesign name='back' size={20}/>
                    </AppButton>
                </View>
                <View style={styles.button}>
                    <AppButton color={THEME.DANGER_COLOR}
                               onPress={() => removeTodo(todo.id)}>
                        <FontAwesome name='remove' size={20} color='#fff'/>
                    </AppButton>
                </View>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    buttons: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button: {
        // width: Dimensions.get('window').width / 3,
        width: Dimensions.get('window').width > 400 ? 150 : 100, //window - size of the visible Application window, screen - size of the device's screen
    },
    card: {
        marginBottom: 20,
        padding: 15
    },
    title: {
        fontSize: 20
    }
});
