import React, {useState, useEffect, useContext, useCallback} from "react";
import {View, StyleSheet, FlatList, Image, Dimensions, useColorScheme} from "react-native";
import {AddTodo} from "../components/AddTodo";
import {Todo} from "../components/Todo";
import {THEME} from "../theme";
import {TodoContext} from "../context/todo/todoContext";
import {ScreenContext} from "../context/screen/screenContext";
import {AppLoader} from "../components/ui/AppLoader";
import {AppText} from "../components/ui/AppText";
import {AppButton} from "../components/ui/AppButton";

export const MainScreen = () => {
    const {addTodo, todos, removeTodo, fetchTodos, loading, error} = useContext(TodoContext);
    const {changeScreen} = useContext(ScreenContext);

    const loadTodos = useCallback(async () => await fetchTodos(), [fetchTodos]);
    useEffect(() => {
        loadTodos();
    }, []); // once when didMount

    const [deviceWidth, setDeviceWidth] = useState(Dimensions.get('window').width - THEME.PADDING_HORISONTAL * 2); //for orientation adaptiveness
    useEffect(() => {
        const update = () => {
            const width = Dimensions.get('window').width - THEME.PADDING_HORISONTAL * 2;
            setDeviceWidth(width);
        };
        Dimensions.addEventListener('change', update);
        return () => { // clear EventListener while willUnmount
            Dimensions.removeEventListener('change', update);
        }
    });

    if (loading) {
        return <AppLoader/>
    }
    if (error) {
        // console.log('Main error', error);
        return (
            <View style={styles.center}>
                <AppText style={styles.error}>{error.error}</AppText>
                <AppButton onPress={loadTodos}>Repeat</AppButton>
            </View>
        )
    }

    let content = (
        <View style={{flex: 1, width: deviceWidth}}>
            <FlatList data={todos} //instead huge ScrollView which needs much memory
                      renderItem={({item}) => <Todo todo={item} onRemove={removeTodo} onOpen={changeScreen}/>}
                      style={{flex: 1}}
                      keyExtractor={item => item.id} // id should be String type for no warnings
            />
        </View>);
    if (todos.length === 0) {
        content = <View style={styles.imgWrap}>
            <Image style={styles.image} source={require('../../assets/original.png')}/>
            {/*<Image style={styles.image} source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png'}} />*/}
        </View>
    }
    return (
        <View style={{flex: 1}}>
            <AddTodo onSubmit={addTodo} style={{flex: 1}}/>
            {content}
        </View>
    )
};

const styles = StyleSheet.create({
    imgWrap: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        height: 300
    },
    image: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain' //prop from docs
    },
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    error: {
        fontSize: 20,
        color: THEME.DANGER_COLOR
    }

});

//Dimensions.get('window').width is used for screen adaptiveness

// <View> //instead <FlatList ...
//     {todos.map(todo => (
//         <Todo todo={todo} key={todo.id}/>
//     ))}
// </View>
// Для компонента FlatList можна задать метод  ListEmptyComponent ,
// который может получаться компонент, который будет рендерится в случае,
// если список пустой. Это очень удобно, не нужно делать проверок на length > 0
