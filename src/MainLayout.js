import React, {useContext} from "react";
import {View, StyleSheet, Alert} from "react-native";
import {Navbar} from "./components/Navbar";
import {THEME} from "./theme";
import {MainScreen} from "./screens/MainScreen";
import {TodoScreen} from "./screens/TodoScreen";
import {ScreenContext} from "./context/screen/screenContext";

export const MainLayout = () => {
    // const {todos, addTodo, removeTodo, updateTodo} = useContext(TodoContext);
    const {todoId} = useContext(ScreenContext);
    // const [todoId, setTodoId] = useState(null); //initialState === null
    // const [todos, setTodos] = useState([]);
    // const addTodo = title => {
    //     // const newTodo = {
    //     //     id: Date.now().toString(),
    //     //     title
    //     // };
    //     // setTodos((prevTodos) => {
    //     //     return [
    //     //         ...prevTodos,
    //     //         newTodo
    //     //     ]})
    //     setTodos(prev => [...prev, {
    //         id: Date.now().toString(), // better when id is String
    //         title
    //     }])
    // };

    // const removeTodo = id => {
    //     const td = todos.find(t => t.id === id);
    //     Alert.alert(
    //         'Todo deletion',
    //         `Do you really want to delete "${td.title}"?`,
    //         [
    //             {
    //                 text: 'Cancel',
    //                 style: 'cancel',
    //             },
    //             {text: 'Delete',
    //                 style: 'destructive', //for iOS only
    //                 onPress: () => {
    //                     setTodoId(null);
    //                     setTodos(prev => prev.filter(todo => todo.id !== id));
    //                 }},
    //         ],
    //         {cancelable: false}, //onpress on darken background behind Alert won't close the Alert
    //     );
    // };

    // const updateTodo = (id, title) => {
    //     setTodos(prev => prev.map(todo => {
    //         if (todo.id === id) {
    //             todo.title = title;
    //         }
    //         return todo;
    //     }));
    // };

    // let content = (
    //     <MainScreen
    //         // todos={todos} addTodo={addTodo} removeTodo={removeTodo} openTodo={changeScreen}
    //     /> //setTodoId and (id)=>setTodoId(id) do the same in openTodo={}
    // );

    // if (todoId) {
    // const selectedTodo = todos.find(todo => todo.id === todoId);
    // content = <TodoScreen
    // goBack={() => changeScreen(null)}
    //                   todo={selectedTodo}
    //                   onRemove={removeTodo}
    //                   onSave={updateTodo}
    // />
    // }
    return (
        <View style={styles.wrapper}>
            <Navbar title="Todo App"/>
            <View style={styles.container}>
                {/*{content}*/}
                {todoId ? <TodoScreen/> : <MainScreen/>}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: THEME.PADDING_HORISONTAL,
        paddingVertical: 20,
        flex: 1
    },
    wrapper: {
        flex: 1
    }
});
