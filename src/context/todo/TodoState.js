import React, {useReducer, useContext} from 'react';
import {Alert} from "react-native";
import {TodoContext} from './todoContext';
import {todoReducer} from "./todoReducer";
import {
    ADD_TODO,
    CLEAR_ERROR,
    FETCH_TODOS,
    HIDE_LOADER,
    REMOVE_TODO,
    SHOW_ERROR,
    SHOW_LOADER,
    UPDATE_TODO
} from "../types";
import {ScreenContext} from "../screen/screenContext";
import {Http} from "../../http";

export const TodoState = ({children}) => {
    const initialState = {
        // todos: [{id: '1', title: 'Learn RN'}]
        todos: [],
        loading: false,
        error: null
    };
    const {changeScreen} = useContext(ScreenContext);
    const [state, dispatch] = useReducer(todoReducer, initialState);

    const addTodo = async title => {
        // const response = await fetch('https://react-native-context-app.firebaseio.com/todos.json', {
        //     method: 'POST',
        //     headers: {'Content-Type': 'application/json'},
        //     body: JSON.stringify({title})
        // });
        // const data = await response.json();
        // // console.log('ID', data.name);
        clearError();
        try {
            const data = await Http.post('https://react-native-context-app.firebaseio.com/todos.json', {title});
            dispatch({type: ADD_TODO, title, id: data.name})
        } catch (e) {
            showError('Something went wrong');
            console.log('AddTodo error', e);
        }
    };
    const removeTodo = id => {
        const td = state.todos.find(t => t.id === id);
        Alert.alert(
            'Todo deletion',
            `Do you really want to delete "${td.title}"?`,
            [
                {
                    text: 'Cancel',
                    style: 'cancel',
                },
                {
                    text: 'Delete',
                    style: 'destructive', //for iOS only
                    onPress: async () => {
                        changeScreen(null);
                        clearError();
                        try {
                            await Http.delete(`https://react-native-context-app.firebaseio.com/todos/${id}.json`);
                            // await fetch(`https://react-native-context-app.firebaseio.com/todos/${id}.json`, {
                            //     method: 'DELETE',
                            //     headers: {'Content-Type': 'application/json'}
                            // });
                            dispatch({type: REMOVE_TODO, id})
                        } catch (e) {
                            showError('Something went wrong');
                            console.log('Delete error', e);
                        }
                    }
                },
            ],
            {cancelable: false}, //onpress on darken background behind Alert won't close the Alert
        );
    };
    const fetchTodos = async () => {
        showLoader();
        clearError();
        try {
            // const response = await fetch('https://react-native-context-app.firebaseio.com/todos.json', {
            //     method: 'GET',
            //     headers: {'Content-Type': 'application/json'},
            // });
            // const data = await response.json();
            // console.log('Fetch data', data);
            const data = await Http.get('https://react-native-context-app.firebaseio.com/todos.json');
            const todos = Object.keys(data).map(key => ({...data[key], id: key})); //transforming data from Firebase DB for correct [] of todos in state
            // setTimeout(() => dispatch({type: FETCH_TODOS, todos}), 2000); //setTimeout for loading testing
            dispatch({type: FETCH_TODOS, todos})
        } catch (e) {
            showError('Something went wrong');
            console.log('Fetch error', e);
        } finally {
            hideLoader(); //in any case is there error or not
        }
    };
    const updateTodo = async (id, title) => {
        clearError();
        try {
            // await fetch(`https://react-native-context-app.firebaseio.com/todos/${id}.json`, {
            //     method: 'PATCH', //PATCH changes only part of DB collection element(here: only title) while PUT changes whole DB collection element
            //     headers: {'Content-Type': 'application/json'},
            //     body: JSON.stringify({title})
            // });
            await Http.patch(`https://react-native-context-app.firebaseio.com/todos/${id}.json`, {title});
            dispatch({type: UPDATE_TODO, id, title});
        } catch (e) {
            showError('Something went wrong');
            console.log('UpdateTodo error', e);
        }
    };

    const showLoader = () => dispatch({type: SHOW_LOADER});
    const hideLoader = () => dispatch({type: HIDE_LOADER});
    const showError = error => dispatch({type: SHOW_ERROR, error});
    const clearError = () => dispatch({type: CLEAR_ERROR});

    return <TodoContext.Provider value={{
        todos: state.todos,
        loading: state.loading,
        error: state.error,
        addTodo, removeTodo, updateTodo,
        fetchTodos
    }}>{children}</TodoContext.Provider>
};
