import {CHANGE_SCREEN} from "../types";

const handlers = {
    [CHANGE_SCREEN]: (state, payload) => payload, //in this case state === payload. We can pass or (state, action.payload) (see 9th code line) or just (state, action)
    DEFAULT: state => state
};
export const screenReducer = (state, action) => {
    const handler = handlers[action.type] || handlers.DEFAULT;
    return handler(state, action.payload);
};
