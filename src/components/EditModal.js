import React, {useState} from "react";
import {View, StyleSheet, Modal, TextInput, Alert} from "react-native";
import {THEME} from "../theme";
import {AppButton} from "./ui/AppButton";

export const EditModal = ({visible, onCancel, value, onSave}) => {
    const [title, setTitle] = useState(value);

    const saveHandler = () => {
        let titleLength = title.trim().length;
        if (titleLength < 3) {
            Alert.alert('Validation error', `The title needs minimum 3 symbols. Now there are ${titleLength} symbols`);
        } else {
            onSave(title);
        }
    };
    const cancelHandler = () => {
        setTitle(value);
        onCancel();
    };
    return (
        <Modal visible={visible} animationType='slide' transparent={false}>
            <View style={styles.wrap}>
                <TextInput value={title}
                           onChangeText={setTitle}
                           style={styles.input}
                           placeholder='Enter new todo name'
                           autoCapitalize='none'
                           autoCorrect={false} maxLength={64}/>
                <View style={styles.buttons}>
                    <AppButton onPress={
                        // onCancel
                        cancelHandler
                    } color={THEME.DANGER_COLOR}>Cancel</AppButton>
                    {/*<Button title="Cancel" onPress={onCancel} color={THEME.DANGER_COLOR}/>*/}
                    <AppButton onPress={saveHandler}>Save</AppButton>
                    {/*<Button title="Save" onPress={() => saveHandler()}/>*/}
                </View>
            </View>
        </Modal>
    )
};

const styles = StyleSheet.create({
    wrap: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    input: {
        padding: 10,
        borderColor: THEME.MAIN_COLOR,
        borderBottomWidth: 2,
        width: '80%',
        marginBottom: 15
    },
    buttons: {
        width: '80%',
        flexDirection: 'row',
        justifyContent: 'space-around'
    }
});
