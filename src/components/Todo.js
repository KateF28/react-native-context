import React from "react";
import {View, StyleSheet, TouchableOpacity} from "react-native";
import {AppText} from "./ui/AppText";

export const Todo = ({todo, onRemove, onOpen}) => {
    return (
        <TouchableOpacity
            activeOpacity={0.5} // level of opacity during press event
            onPress={() => onOpen(todo.id)}
            onLongPress={() => onRemove(todo.id)} //not just onRemovetodo.id) without callback for handling onRemove on pressing event but not just on code readiness
        >
            <View style={styles.todo}>
                <AppText
                    // style={styles.title}
                >{todo.title}</AppText>
            </View>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    todo: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 15,
        borderWidth: 1,
        borderColor: '#eee',
        borderRadius: 5,
        marginBottom: 5,
    },
    // title: {
    //     fontFamily: 'roboto-bold'
    // }
});
