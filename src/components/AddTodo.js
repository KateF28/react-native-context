import React, {useState} from "react";
import {View, StyleSheet, TextInput, Alert, Keyboard} from 'react-native';
import {AntDesign} from '@expo/vector-icons';
import {THEME} from "../theme";

export const AddTodo = ({onSubmit}) => {
    const [value, setValue] = useState('');

    const pressHandler = () => {
        if (value.trim()) {
            onSubmit(value);
            setValue(''); // clear input value
            Keyboard.dismiss(); //hides keyboard !!!iOS has type of keyboard which doesn't have it's close btn
        } else {
            Alert.alert("A todo title can't be empty");
        }
    };

    return (
        <View style={styles.block}>
            <TextInput style={styles.input}
                       onChangeText={setValue} // In this simple case we can right not text => setValue(text) but just setValue
                       value={value}
                       placeholder='Enter a todo title'
                       autoCorrect={false} //Keyboard Android prop
                       autoCapitalize='none'  //Keyboard Android prop. 'sentences' prop value is by default (first letter of each sentence is capitalized)
                // keyboardType='number-pad' //says which keyboard to open
            />
            <AntDesign.Button onPress={pressHandler} name='pluscircleo'>Add</AntDesign.Button>
            {/*<Button title='Add' onPress={pressHandler}/>*/}
        </View>
    )
};

const styles = StyleSheet.create({
    block: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 15,
    },
    input: {
        width: '75%',
        padding: 10,
        borderStyle: 'solid',
        borderBottomWidth: 2,
        borderBottomColor: THEME.MAIN_COLOR,
    },
});
